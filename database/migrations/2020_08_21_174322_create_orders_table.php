<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->id();
            $table->string('title')->unique();
            $table->string('slug')->unique();
            $table->bigInteger('subject_id')->unsigned();
            $table->bigInteger('academinc_level_id')->unisigned();
            $table->bigInteger('work_type_id')->unsigned();
            $table->bigInteger('citation_style_id')->unisigned();
            $table->bigInteger('customer_id')->unsigned();
            $table->string('urgency');
            $table->string('writer_level');
            $table->integer('number_of_pasges');
            $table->json('attachements');
            $table->string('status')->default('proccessing');
            $table->boolean('paid');
            $table->float('price')->default(20);
            $table->longText('description');
            $table->string('order_no')->unique();
            $table->boolean('vip')->default(false);
            $table->softDeletes();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
