<?php

use Illuminate\Database\Seeder;

class UserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $data = [
            'first_name' => 'Odhiambo',
            'last_name' => 'Dormnic',
            'username' => "ayimdomnic",
            'country_code' => '+254',
            'phone' => '0725554415',
            'email' => 'ayimdomnic@gmail.com',
            'password' => bcrypt('password')
        ];

        $user = \App\User::create($data);

    }
}
