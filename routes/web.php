<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('landing');
});

Route::get('/app', function () {
    return view('welcome');
});

Route::get('/order', 'HomeController@order')->name('order');
Route::get('/faq', 'HomeController@faq')->name('faq');
Route::get('/how-it-works', 'HomeController@howItWorks')->name('hiw');
Route::get('/sample', 'HomeController@sample')->name('samples');
Route::get('/contact', 'HomeController@contact')->name('contact');
Route::get('/pricing', 'HomeController@pricing')->name('pricing');
Route::get('/about', 'HomeController@about')->name('about');

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');

Route::group(['prefix' => 'admin', 'as' => 'admin.', 'namespace' => 'Admin\\'], function (){
    Route::get('dashboard', 'DashboardController@index')->name('dashboard');
    Route::resource('subjects', 'SubjectsController');
    Route::resource('orders', 'OrdersController');
    Route::resource('academic_levels', 'AccademicLevelsController');
    Route::resource('logs', 'LogsController');
    Route::resource('work_types', 'WorkTypesController');
    Route::resource('customers', 'CustomersController');
    Route::resource('citation_styles', 'CitationStylesController');
});
