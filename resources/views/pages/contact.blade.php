@extends('layouts.app')


@section('content')

<main class="contant">
    <div class="def-width">
        <h1 class="title">Contact</h1>
        <p>Have any questions? We’d love to hear from you.Here’s how to get in touch with us.</p>
        <ul class="contant__items flex">
            <li>
                <div class="green-icon">
                    <img src="../images/svg/iconouroffice.svg" alt="">
                </div>
                <div class="contact__text">
                    <h2>Our office</h2>
                    <p><a href="#">Site.com</a> company is situated in London and offers academic assistance for all students. Our address is SUITE 7104, 43 BEDFORD STREET, LONDON, ENGLAND, WC2E 9HA.</p>
                </div>
            </li>
            <li>
                <div class="orange-icon">
                    <img src="../images/svg/iconchat.svg" alt="">
                </div>
                <div class="contact__text">
                    <h2>Chat</h2>
                    <p>Available on every page of our website.</p>
                </div>
            </li>
            <li>
                <div class="red-icon">
                    <img src="../images/svg/iconmail.svg" alt="">
                </div>
                <div class="contact__text">
                    <h2>E-mail</h2>
                    <p><a href="mailto:support@smartessay.org">support@smartessay.org</a></p>
                </div>
            </li>
        </ul>
    </div>
</main>
@endsection