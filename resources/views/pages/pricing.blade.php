@extends('layouts.app')


@section('content')
<main class="pricing-cont">
	<div class="def-width with-sidebar">
		<div class="pricing-cont__text near-sidebar">
			<h1 class="title">Pricing</h1>
			<p>Pricing per page is determined by thee different factors and is based on your unique needs as a customer. <br>
			Read below to learn how the price is calculated at EssayAssist</p>
			<ul>
				<li><img src="../images/svg/icon_level.svg" alt="">Academic level</li>
				<li><img src="../images/svg/icon_notebook.svg" alt="">Type of assignment</li>
				<li><img src="../images/svg/icon_clock.svg" width="24" alt="">Urgency</li>
			</ul>
			<div class="pricing-cont__tabs-wrapper">
				<div class="pricing-cont__tabs-cont">
					<ul class="pricing-cont__tabs">
						<li class="active">Writing from scratch</li>
						<li>Editing/proofreading</li>
						<li>Problem solving</li>
					</ul>
					<ul class="pricing-cont__tab">
					<li class="active">
						<table>
							<tr>
								<th>Deadline</th>
								<th>High School</th>
								<th>Undergraduate (yrs 1-2)</th>
								<th>Undergraduate (yrs 3-4)</th>
								<th>Master's</th>
								<th>Doctoral</th>
								<th>Admissions</th>
							</tr>
							<tr>
								<td>14 Days</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>9 Days</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>7 Days</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>5 Days</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>3 Days</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>2 Days</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>24 Hours</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>12 Hours</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
							<tr>
								<td>6 Hours</td>
								<td>$8</td>
								<td>$10</td>
								<td>$14</td>
								<td>$19</td>
								<td>$19</td>
								<td>$14</td>
							</tr>
						</table>
					</li>
					<li>
						<table>
							<tr>
								<th>Deadline</th>
								<th>High School</th>
								<th>Undergraduate (yrs 1-2)</th>
								<th>Undergraduate (yrs 3-4)</th>
								<th>Master's</th>
								<th>Doctoral</th>
								<th>Admissions</th>
							</tr>
							<tr>
								<td>14 Days</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>9 Days</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>7 Days</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>5 Days</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>3 Days</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>2 Days</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>24 Hours</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>12 Hours</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
							<tr>
								<td>6 Hours</td>
								<td>$6</td>
								<td>$8</td>
								<td>$10</td>
								<td>$13</td>
								<td>$15</td>
								<td>$17</td>
							</tr>
						</table>
					</li>
					<li>
						<table>
							<tr>
								<th>Deadline</th>
								<th>High School</th>
								<th>Undergraduate (yrs 1-2)</th>
								<th>Undergraduate (yrs 3-4)</th>
								<th>Master's</th>
								<th>Doctoral</th>
								<th>Admissions</th>
							</tr>
							<tr>
								<td>14 Days</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>9 Days</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>7 Days</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>5 Days</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>3 Days</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>2 Days</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>24 Hours</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>12 Hours</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
							<tr>
								<td>6 Hours</td>
								<td>$100</td>
								<td>$110</td>
								<td>$112</td>
								<td>$113</td>
								<td>$114</td>
								<td>$119</td>
							</tr>
						</table>
					</li>
				</ul>
				</div>
			</div>
		</div>
		<aside class="sidebar">
	<div class="sidebar__calc sidebar__item sidebar__item--blue">
		<div class="calculator">
			<form action="">
				<div class="calculator-top">
					<h3>Calculate the price</h3>
				</div>
				<div class="calculator-cont">
					<div class="select-style">
						<select name="" id="">
							<option value="">Academic level</option>
							<option value="">Academic level 2</option>
							<option value="">Academic level 3</option>
						</select>
					</div>
					<div class="select-style">
						<select name="" id="">
							<option value="">Type of Assigment</option>
							<option value="">Type of Assigment 2</option>
							<option value="">Type of Assigment 3</option>
						</select>
					</div>
					<div class="select-style">
						<select name="" id="">
							<option value="">Deadline</option>
							<option value="">Deadline 2</option>
							<option value="">Deadline 3</option>
						</select>
					</div>
				</div>
				<div class="calculator-pages">
					<p>Pages:</p>
					<input type="text" placeholder="1">
					<p>Word count: 275</p>
				</div>
				<div class="calculator-bottom">
					<span>$19,50</span>
					<button type="submit" class="butn butn-red">Continue order</button>
				</div>
			</form>
		</div>
	</div>
	<div class="sidebar__review sidebar__item sidebar__item--red">
		<h3>Users reviews</h3>
		<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
		<div class="rating-stars">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star2.svg" alt="">
		</div>
		<div class="sidebar__review-logos">
			<div><img src="../images/logotustpilot.png" width="92" height="38" class="retina" alt=""></div>
			<div><img src="../images/logositejabber.png" width="114" height="16" class="retina" alt=""></div>
		</div>
	</div>
	<div class="sidebar__email sidebar__item sidebar__item--dark">
		<h3>Email  newsletter</h3>
		<p>Subscribe to receive inspiration, ideas, and news in your inbox.</p>
		<form action="">
			<input type="text" placeholder="Email Address">
			<button class="butn butn-red">Sing in</button>
		</form>
	</div>
	<div class="sidebar__mcafee sidebar__item sidebar__item--blue">
		<h3>McAfee Certified</h3>
		<div class="sidebar__mcafee-cont">
			<div><img src="../images/Logo_MCAfee.png" width="82px" height="29" class="retina" alt=""></div>
			<a href="#">click to verify</a>
		</div>
	</div>
	<div class="sidebar__accept sidebar__item sidebar__item--red">
		<h3>We accept</h3>
		<ul>
			<li><img src="../images/svg/icon_visa_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_mastercard_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_pay_pal_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_american_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_diccover_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_JCB_2.svg" alt=""></li>
		</ul>
	</div>
	<div class="sidebar__secure sidebar__item sidebar__item--dark">
		<h3><img src="../images/svg/icon_castle2.svg" alt="">Secure Shopping Cart</h3>
		<p>Your transaction is securely processed using <span>256-Bit</span> SSL Encyption with PayPal or any major debit or credit card. <br>
		We do not store or have access to your payment data.</p>
		<h3><img src="../images/svg/icon_security.svg" alt="">Privacy & Security</h3>
		<p>Yor personal information is private, <strong>confidential</strong> and will not be shares with anyone. not even our writers.</p>
	</div>
	<div class="sidebar__inclus sidebar__item sidebar__item--dark">
		<h3>Free inclusions</h3>
		<ul class="check-list">
			<li>Revisions and corrections</li>
			<li>Title page</li>
			<li>References page</li>
			<li>Formatiing</li>
		</ul>
	</div>
	<div class="sidebar__reason sidebar__item sidebar__item--dark">
		<h3>Reasons to choose us</h3>
		<ul>
			<li>
				<div><img src="../images/svg/icon_clock2.svg" width="23" height="23" alt=""></div>
				<p>24/7 support</p>
			</li>
			<li>
				<div><img src="../images/svg/icon_search.svg" width="23" height="23" alt=""></div>
				<p>Original content</p>
			</li>
			<li>
				<div><img src="../images/svg/icon_graduation_cap.svg" alt=""></div>
				<p>Any topic & any difficulty</p>
			</li>
			<li>
				<div><img src="../images/svg/castle.svg" width="23" alt=""></div>
				<p>Confidentiality & security</p>
			</li>
		</ul>
	</div>
</aside>

	</div>
</main>

@endsection