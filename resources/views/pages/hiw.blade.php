@extends('layouts.app')


@section('content')

<main class="hiw-cont">
	<div class="def-width with-sidebar">
		<div class="hiw-cont__text near-sidebar">
			<h1 class="title">How it works</h1>
			<p>Getting professional academic help from us is easy. Get your custom written paper done for you in 3 simple steps:</p>
			<ul>
				<li>
					<div class="hiw-cont__item-img"><img src="../images/svg/icon_1.svg" alt=""></div>
					<div class="hiw-cont__item-text">
						<h3>Fill in the order form</h3>
						<p>Placing an order has never been so fast and easy. Our single page application website ensures a supreme speed of all your operations. Just press the “Order now” button and let the system intuitively guide you through the process. Submit the paper details, upload files, and provide contact information – you are almost done!</p>
					</div>
				</li>
				<li>
					<div class="hiw-cont__item-img"><img src="../images/svg/icon_2.svg" alt=""></div>
					<div class="hiw-cont__item-text">
						<h3>We assign the writer</h3>
						<p>There is even less you need to do at this point – you are the sole maker of the price you pay! No hidden cost. The number of pages, academic level and deadline determine the price. Facing problems with the price of your paper? Just contact us in chat and we will definitely come to a reasonable conclusion.</p>
					</div>
				</li>
				<li>
					<div class="hiw-cont__item-img"><img src="../images/svg/icon_3.svg" alt=""></div>
					<div class="hiw-cont__item-text">
						<h3>Writer makes a research and writes your paper</h3>
						<p>All payments are being processed by secure Payment system. We do not store your credit card information. This enables us to guarantee a 100% security of your funds and process payments swiftly. Having troubles submitting your payment details? We are available 24/7 in chat to help you!</p>
					</div>
				</li>
				<li>
					<div class="hiw-cont__item-img"><img src="../images/svg/icon_4.svg" alt=""></div>
					<div class="hiw-cont__item-text">
						<h3>We send the completed paper to you</h3>
						<p>After the writer completes the paper you will receive an email asking to check the preview version of it. Just log in to your account and check if you are satisfied with the work done. You can either approve the paper and get a full word.doc document in the file section, or ask the writer to revise it by pressing the “Send for revision” button.</p>
					</div>
				</li>
				<li>
					<div class="hiw-cont__item-img"><img src="../images/svg/icon_5.svg" alt=""></div>
					<div class="hiw-cont__item-text">
						<h3>Revisions</h3>
						<p>Placing an order has never been so fast and easy. Our single page application website ensures a supreme speed of all your operations. Just press the “Order now” button and let the system intuitively guide you through the process. Submit the paper details, upload files, and provide contact information – you are almost done!</p>
					</div>
				</li>
			</ul>
		</div>
		<aside class="sidebar">
	<div class="sidebar__calc sidebar__item sidebar__item--blue">
		<div class="calculator">
			<form action="">
				<div class="calculator-top">
					<h3>Calculate the price</h3>
				</div>
				<div class="calculator-cont">
					<div class="select-style">
						<select name="" id="">
							<option value="">Academic level</option>
							<option value="">Academic level 2</option>
							<option value="">Academic level 3</option>
						</select>
					</div>
					<div class="select-style">
						<select name="" id="">
							<option value="">Type of Assigment</option>
							<option value="">Type of Assigment 2</option>
							<option value="">Type of Assigment 3</option>
						</select>
					</div>
					<div class="select-style">
						<select name="" id="">
							<option value="">Deadline</option>
							<option value="">Deadline 2</option>
							<option value="">Deadline 3</option>
						</select>
					</div>
				</div>
				<div class="calculator-pages">
					<p>Pages:</p>
					<input type="text" placeholder="1">
					<p>Word count: 275</p>
				</div>
				<div class="calculator-bottom">
					<span>$19,50</span>
					<button type="submit" class="butn butn-red">Continue order</button>
				</div>
			</form>
		</div>
	</div>
	<div class="sidebar__review sidebar__item sidebar__item--red">
		<h3>Users reviews</h3>
		<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
		<div class="rating-stars">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star2.svg" alt="">
		</div>
		<div class="sidebar__review-logos">
			<div><img src="../images/logotustpilot.png" width="92" height="38" class="retina" alt=""></div>
			<div><img src="../images/logositejabber.png" width="114" height="16" class="retina" alt=""></div>
		</div>
	</div>
	<div class="sidebar__email sidebar__item sidebar__item--dark">
		<h3>Email  newsletter</h3>
		<p>Subscribe to receive inspiration, ideas, and news in your inbox.</p>
		<form action="">
			<input type="text" placeholder="Email Address">
			<button class="butn butn-red">Sing in</button>
		</form>
	</div>
	<div class="sidebar__mcafee sidebar__item sidebar__item--blue">
		<h3>McAfee Certified</h3>
		<div class="sidebar__mcafee-cont">
			<div><img src="../images/Logo_MCAfee.png" width="82px" height="29" class="retina" alt=""></div>
			<a href="#">click to verify</a>
		</div>
	</div>
	<div class="sidebar__accept sidebar__item sidebar__item--red">
		<h3>We accept</h3>
		<ul>
			<li><img src="../images/svg/icon_visa_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_mastercard_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_pay_pal_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_american_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_diccover_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_JCB_2.svg" alt=""></li>
		</ul>
	</div>
	<div class="sidebar__secure sidebar__item sidebar__item--dark">
		<h3><img src="../images/svg/icon_castle2.svg" alt="">Secure Shopping Cart</h3>
		<p>Your transaction is securely processed using <span>256-Bit</span> SSL Encyption with PayPal or any major debit or credit card. <br>
		We do not store or have access to your payment data.</p>
		<h3><img src="../images/svg/icon_security.svg" alt="">Privacy & Security</h3>
		<p>Yor personal information is private, <strong>confidential</strong> and will not be shares with anyone. not even our writers.</p>
	</div>
	<div class="sidebar__inclus sidebar__item sidebar__item--dark">
		<h3>Free inclusions</h3>
		<ul class="check-list">
			<li>Revisions and corrections</li>
			<li>Title page</li>
			<li>References page</li>
			<li>Formatiing</li>
		</ul>
	</div>
	<div class="sidebar__reason sidebar__item sidebar__item--dark">
		<h3>Reasons to choose us</h3>
		<ul>
			<li>
				<div><img src="../images/svg/icon_clock2.svg" width="23" height="23" alt=""></div>
				<p>24/7 support</p>
			</li>
			<li>
				<div><img src="../images/svg/icon_search.svg" width="23" height="23" alt=""></div>
				<p>Original content</p>
			</li>
			<li>
				<div><img src="../images/svg/icon_graduation_cap.svg" alt=""></div>
				<p>Any topic & any difficulty</p>
			</li>
			<li>
				<div><img src="../images/svg/castle.svg" width="23" alt=""></div>
				<p>Confidentiality & security</p>
			</li>
		</ul>
	</div>
</aside>

	</div>
</main>
@endsection