@extends('layouts.app')


@section('content')

<main class="faq-cont">
	<div class="def-width with-sidebar">
		<div class="faq-cont__text near-sidebar">
			<h1 class="title">FAQ</h1>
			<ul class="faq-cont__accordion">
				<li class="faq-cont__item faq-cont__item--active">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">Why should I choose you to complete my paper?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, dolore tempora libero ducimus animi deleniti eius quod perspiciatis facilis, voluptate officiis quasi neque fuga, explicabo natus officia repellendus porro harum.</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">Is your service confidential?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga in eum minima obcaecati adipisci, labore et cum amet! Non rem, autem deserunt nesciunt placeat debitis at eveniet vero unde tenetur.</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">I need my paper in the latest edition of APA, can you do that?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ad corporis ipsum ut. Veniam eligendi aperiam excepturi, facere architecto. Vitae laudantium perspiciatis nostrum pariatur ipsum, atque quia obcaecati debitis odio.</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">Who will be working on my paper?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet ipsam porro perferendis aliquid vero, eligendi impedit aspernatur incidunt ut voluptates, illum adipisci, nostrum voluptatem voluptas fugiat, eveniet obcaecati dolores aliquam modi! Autem sed consequuntur, ducimus sequi, illum tempore explicabo. Nulla natus impedit quasi accusamus odio quae atque dolor. Nisi laudantium, nulla dolores tempora aliquid eligendi. Rerum animi cumque deserunt adipisci eum velit, fuga error, mollitia vero quod tempore iusto culpa neque ab expedita placeat reiciendis doloremque, ullam nobis molestiae et maxime quasi perferendis. Et laudantium necessitatibus eius tempora vel distinctio, blanditiis animi hic deleniti ratione fugiat eos optio architecto saepe!</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">Why should I choose you to complete my paper?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Nostrum, dolore tempora libero ducimus animi deleniti eius quod perspiciatis facilis, voluptate officiis quasi neque fuga, explicabo natus officia repellendus porro harum.</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">Is your service confidential?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Fuga in eum minima obcaecati adipisci, labore et cum amet! Non rem, autem deserunt nesciunt placeat debitis at eveniet vero unde tenetur.</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">I need my paper in the latest edition of APA, can you do that?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ad corporis ipsum ut. Veniam eligendi aperiam excepturi, facere architecto. Vitae laudantium perspiciatis nostrum pariatur ipsum, atque quia obcaecati debitis odio.</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">Who will be working on my paper?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet ipsam porro perferendis aliquid vero, eligendi impedit aspernatur incidunt ut voluptates, illum adipisci, nostrum voluptatem voluptas fugiat, eveniet obcaecati dolores aliquam modi! Autem sed consequuntur, ducimus sequi, illum tempore explicabo. Nulla natus impedit quasi accusamus odio quae atque dolor. Nisi laudantium, nulla dolores tempora aliquid eligendi. Rerum animi cumque deserunt adipisci eum velit, fuga error, mollitia vero quod tempore iusto culpa neque ab expedita placeat reiciendis doloremque, ullam nobis molestiae et maxime quasi perferendis. Et laudantium necessitatibus eius tempora vel distinctio, blanditiis animi hic deleniti ratione fugiat eos optio architecto saepe!</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">I need my paper in the latest edition of APA, can you do that?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Maxime ad corporis ipsum ut. Veniam eligendi aperiam excepturi, facere architecto. Vitae laudantium perspiciatis nostrum pariatur ipsum, atque quia obcaecati debitis odio.</p>
				</li>
				<li class="faq-cont__item">
					<p class="faq-cont__question">
						<span class="faq-cont__question-link">Who will be working on my paper?</span>
						<i class="faq-cont__question-icon">
							<svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="16" height="8" viewBox="0 0 8 4">
								<path id="faq-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#282828" />
							</svg>
						</i>
					</p>
					<p class="faq-cont__answer">Lorem ipsum dolor sit amet, consectetur adipisicing elit. Amet ipsam porro perferendis aliquid vero, eligendi impedit aspernatur incidunt ut voluptates, illum adipisci, nostrum voluptatem voluptas fugiat, eveniet obcaecati dolores aliquam modi! Autem sed consequuntur, ducimus sequi, illum tempore explicabo. Nulla natus impedit quasi accusamus odio quae atque dolor. Nisi laudantium, nulla dolores tempora aliquid eligendi. Rerum animi cumque deserunt adipisci eum velit, fuga error, mollitia vero quod tempore iusto culpa neque ab expedita placeat reiciendis doloremque, ullam nobis molestiae et maxime quasi perferendis. Et laudantium necessitatibus eius tempora vel distinctio, blanditiis animi hic deleniti ratione fugiat eos optio architecto saepe!</p>
				</li>
			</ul>
		</div>
		<aside class="sidebar">
	<div class="sidebar__calc sidebar__item sidebar__item--blue">
		<div class="calculator">
			<form action="">
				<div class="calculator-top">
					<h3>Calculate the price</h3>
				</div>
				<div class="calculator-cont">
					<div class="select-style">
						<select name="" id="">
							<option value="">Academic level</option>
							<option value="">Academic level 2</option>
							<option value="">Academic level 3</option>
						</select>
					</div>
					<div class="select-style">
						<select name="" id="">
							<option value="">Type of Assigment</option>
							<option value="">Type of Assigment 2</option>
							<option value="">Type of Assigment 3</option>
						</select>
					</div>
					<div class="select-style">
						<select name="" id="">
							<option value="">Deadline</option>
							<option value="">Deadline 2</option>
							<option value="">Deadline 3</option>
						</select>
					</div>
				</div>
				<div class="calculator-pages">
					<p>Pages:</p>
					<input type="text" placeholder="1">
					<p>Word count: 275</p>
				</div>
				<div class="calculator-bottom">
					<span>$19,50</span>
					<button type="submit" class="butn butn-red">Continue order</button>
				</div>
			</form>
		</div>
	</div>
	<div class="sidebar__review sidebar__item sidebar__item--red">
		<h3>Users reviews</h3>
		<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
		<div class="rating-stars">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star.svg" alt="">
			<img src="../images/svg/icon_star2.svg" alt="">
		</div>
		<div class="sidebar__review-logos">
			<div><img src="../images/logotustpilot.png" width="92" height="38" class="retina" alt=""></div>
			<div><img src="../images/logositejabber.png" width="114" height="16" class="retina" alt=""></div>
		</div>
	</div>
	<div class="sidebar__email sidebar__item sidebar__item--dark">
		<h3>Email  newsletter</h3>
		<p>Subscribe to receive inspiration, ideas, and news in your inbox.</p>
		<form action="">
			<input type="text" placeholder="Email Address">
			<button class="butn butn-red">Sing in</button>
		</form>
	</div>
	<div class="sidebar__mcafee sidebar__item sidebar__item--blue">
		<h3>McAfee Certified</h3>
		<div class="sidebar__mcafee-cont">
			<div><img src="../images/Logo_MCAfee.png" width="82px" height="29" class="retina" alt=""></div>
			<a href="#">click to verify</a>
		</div>
	</div>
	<div class="sidebar__accept sidebar__item sidebar__item--red">
		<h3>We accept</h3>
		<ul>
			<li><img src="../images/svg/icon_visa_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_mastercard_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_pay_pal_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_american_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_diccover_2.svg" alt=""></li>
			<li><img src="../images/svg/icon_JCB_2.svg" alt=""></li>
		</ul>
	</div>
	<div class="sidebar__secure sidebar__item sidebar__item--dark">
		<h3><img src="../images/svg/icon_castle2.svg" alt="">Secure Shopping Cart</h3>
		<p>Your transaction is securely processed using <span>256-Bit</span> SSL Encyption with PayPal or any major debit or credit card. <br>
		We do not store or have access to your payment data.</p>
		<h3><img src="../images/svg/icon_security.svg" alt="">Privacy & Security</h3>
		<p>Yor personal information is private, <strong>confidential</strong> and will not be shares with anyone. not even our writers.</p>
	</div>
	<div class="sidebar__inclus sidebar__item sidebar__item--dark">
		<h3>Free inclusions</h3>
		<ul class="check-list">
			<li>Revisions and corrections</li>
			<li>Title page</li>
			<li>References page</li>
			<li>Formatiing</li>
		</ul>
	</div>
	<div class="sidebar__reason sidebar__item sidebar__item--dark">
		<h3>Reasons to choose us</h3>
		<ul>
			<li>
				<div><img src="../images/svg/icon_clock2.svg" width="23" height="23" alt=""></div>
				<p>24/7 support</p>
			</li>
			<li>
				<div><img src="../images/svg/icon_search.svg" width="23" height="23" alt=""></div>
				<p>Original content</p>
			</li>
			<li>
				<div><img src="../images/svg/icon_graduation_cap.svg" alt=""></div>
				<p>Any topic & any difficulty</p>
			</li>
			<li>
				<div><img src="../images/svg/castle.svg" width="23" alt=""></div>
				<p>Confidentiality & security</p>
			</li>
		</ul>
	</div>
</aside>

	</div>
</main>
@endsection