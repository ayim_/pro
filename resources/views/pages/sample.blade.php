@extends('layouts.app')


@section('content')

<main class="samples">
    <div class="def-width with-sidebar">
        <div class="samples__left near-sidebar">
            <h1 class="title">Samples</h1>
            <ul class="samples__items">
                <li>
                    <div class="samples__text">
                        <h2>Essay</h2>
                        <p><strong>Paper title:</strong> <a href="#">How technology will change our lives in twenty</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> MLA</li>
                            <li><strong>Sources:</strong> 3</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Case study</h2>
                        <p><strong>Paper title:</strong> <a href="#">Birth and Nature</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> Chicago</li>
                            <li><strong>Sources:</strong> 2</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Presentation / Speech</h2>
                        <p><strong>Paper title:</strong> <a href="#">Presentation / Speech</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> Chicago</li>
                            <li><strong>Sources:</strong> 5</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Essay</h2>
                        <p><strong>Paper title:</strong> <a href="#">How technology will change our lives in twenty</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> MLA</li>
                            <li><strong>Sources:</strong> 3</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Case study</h2>
                        <p><strong>Paper title:</strong> <a href="#">Birth and Nature</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> Chicago</li>
                            <li><strong>Sources:</strong> 2</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Presentation / Speech</h2>
                        <p><strong>Paper title:</strong> <a href="#">Presentation / Speech</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> Chicago</li>
                            <li><strong>Sources:</strong> 5</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Essay</h2>
                        <p><strong>Paper title:</strong> <a href="#">How technology will change our lives in twenty</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> MLA</li>
                            <li><strong>Sources:</strong> 3</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Case study</h2>
                        <p><strong>Paper title:</strong> <a href="#">Birth and Nature</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> Chicago</li>
                            <li><strong>Sources:</strong> 2</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Presentation / Speech</h2>
                        <p><strong>Paper title:</strong> <a href="#">Presentation / Speech</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> Chicago</li>
                            <li><strong>Sources:</strong> 5</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
                <li>
                    <div class="samples__text">
                        <h2>Essay</h2>
                        <p><strong>Paper title:</strong> <a href="#">How technology will change our lives in twenty</a></p>
                        <ul>
                            <li><strong>Academic level:</strong> College</li>
                            <li><strong>Discipline:</strong> Education</li>
                            <li><strong>Paper Format:</strong> MLA</li>
                            <li><strong>Sources:</strong> 3</li>
                        </ul>
                    </div>
                    <div class="samples__img">
                        <a href="#"><img src="../images/svg/iconpdf.svg" alt="">View this samples</a>
                    </div>
                </li>
            </ul>
        </div>
        <aside class="sidebar">
<div class="sidebar__calc sidebar-item">
    <h2>Calculate Your Price</h2>
    <form action="">
        <ul>
            <li>
                <div class="select-style">
                    <select name="" id="">
                        <option value="">Academic level</option>
                        <option value="">Academic level 2</option>
                        <option value="">Academic level 3</option>
                    </select>
                </div>
            </li>
            <li>
                <div class="select-style">
                    <select name="" id="">
                        <option value="">Type of paper</option>
                        <option value="">Type of paper 2</option>
                        <option value="">Type of paper 3</option>
                    </select>
                </div>
            </li>
            <li class="sidebar__calc-dline">
                <div class="select-style">
                    <select name="" id="">
                        <option value="">Deadline</option>
                        <option value="">Deadline 2</option>
                        <option value="">Deadline 3</option>
                    </select>
                </div>
            </li>
            <li class="sidebar__calc-pages">
                <input type="text" placeholder="Pages">
                <span>(Word count 275)</span>
            </li>
        </ul>
        <div class="sidebar__calc-butn">
            <button type="submit" class="butn butn-orange">Continue</button>
            <h2>Price <span>14$</span></h2>
        </div>
    </form>
</div>
<div class="sidebar__testim sidebar-item">
    <h2>Testimonials</h2>
    <div class="sidebar__testim-cust">
        <div class="cust-icon"><img src="../images/svg/iconcustomers.svg" alt=""></div>
        <div>
            <h3>Customer ID: 2379</h3>
            <span>
                <img src="../images/svg/iconstar.svg" alt="">
                <img src="../images/svg/iconstar.svg" alt="">
                <img src="../images/svg/iconstar.svg" alt="">
                <img src="../images/svg/iconstar.svg" alt="">
                <img src="../images/svg/iconstar.svg" alt="">
            </span>
        </div>

    </div>
    <p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
    <a href="#">Read the latest testimonials</a>
</div>
<div class="sidebar__why sidebar-item">
    <h2>Why choose us?</h2>
    <ul>
        <li>
            <div class="orange-icon">
                <img src="../images/svg/iconsubject.svg" alt="">
            </div>
            Any subject
        </li>
        <li>
            <div class="red-icon">
                <img src="../images/svg/icondiscipline.svg" alt="">
            </div>
            Any discipline
        </li>
        <li>
            <div class="blue-icon">
                <img src="../images/svg/icontopic.svg" alt="">
            </div>
            Any type of paper
        </li>
    </ul>
</div>
<div class="sidebar__stat sidebar-item">
    <h2>Our statistics</h2>
    <ul>
        <li>
            <img src="../images/svg/iconcap.svg" alt="">
            <p><span>97.3% </span>satisfaction rate </p>
        </li>
        <li>
            <img src="../images/svg/iconcap.svg" alt="">
            <p><span>9/10 </span>average quality score</p>
        </li>
        <li>
            <img src="../images/svg/iconcap.svg" alt="">
            <p><span>98.8% </span>of papers delivered on time </p>
        </li>
        <li>
            <img src="../images/svg/iconcap.svg" alt="">
            <p><span>78.6% </span>of customers reorder</p>
        </li>
    </ul>
</div>
</aside>

    </div>
</main>
@endsection