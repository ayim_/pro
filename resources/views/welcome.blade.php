@extends('layouts.app')

@section('content')

<main class="home-cont">
	<section class="home-cont__lid">
		<div class="def-width">
			<div class="home-cont__lid-text">
				<h2>Quick Academic Help</h2>
				<p>Get an expert academic writing assistance! We can write any paper on any subject within the tightest deadline.</p>
			</div>
			<div class="home-cont__lid-calc">
				<div class="calculator">
					<form action="">
						<div class="calculator-top">
							<h3>Calculate the price</h3>
							<div><img src="images/Logo_MCAfee.png" width="82" height="29" class="retina" alt=""></div>
						</div>
						<div class="calculator-cont">
							<div class="select-style">
								<select name="" id="">
									<option value="">Academic level</option>
									<option value="">Academic level 2</option>
									<option value="">Academic level 3</option>
								</select>
							</div>
							<div class="select-style">
								<select name="" id="">
									<option value="">Type of Assigment</option>
									<option value="">Type of Assigment 2</option>
									<option value="">Type of Assigment 3</option>
								</select>
							</div>
							<div class="select-style">
								<select name="" id="">
									<option value="">Deadline</option>
									<option value="">Deadline 2</option>
									<option value="">Deadline 3</option>
								</select>
							</div>
						</div>
						<div class="calculator-pages">
							<p>Pages:</p>
							<input type="text" placeholder="1">
							<p>Word count: 275</p>
						</div>
						<div class="calculator-bottom">
							<span>$19,50</span>
							<button type="submit" class="butn butn-red">Continue order</button>
						</div>
					</form>
				</div>
			</div>
		</div>
	</section>
	<section class="home-cont__why">
		<div class="def-width">
			<h2 class="like-h1 title">Why us?</h2>
			<ul>
				<li>
					<div class="home-cont__why-img">
						<img src="images/svg/icon_quality.svg" alt="">
					</div>
					<div class="home-cont__why-text">
						<h3>Quality <i class="show-in-mob"></i></h3>
						<p>You get an original and high-quality paper based on extensive research. The completed work will be correctly formatted, referenced and tailored to your level of study.</p>
					</div>
				</li>
				<li>
					<div class="home-cont__why-img">
						<img src="images/svg/icon_security.svg" alt="">
					</div>
					<div class="home-cont__why-text">
						<h3>Confidentiality <i class="show-in-mob"></i></h3>
						<p>We value your privacy. We do not disclose your personal information to any third party without your consent. Your payment data is also safely handled as you process the payment through a secured and verified payment processor.</p>
					</div>
				</li>
				<li>
					<div class="home-cont__why-img">
						<img src="images/svg/icon_desktop.svg" alt="">
					</div>
					<div class="home-cont__why-text">
						<h3>Originality <i class="show-in-mob"></i></h3>
						<p>Every single order we deliver is written from scratch according to your instructions. We have zero tolerance for plagiarism, so all completed papers are unique and checked for plagiarism using a leading plagiarism detector.</p>
					</div>
				</li>
				<li>
					<div class="home-cont__why-img">
						<img src="images/svg/icon_clock.svg" alt="">
					</div>
					<div class="home-cont__why-text">
						<h3>On-time delivery <i class="show-in-mob"></i></h3>
						<p>We strive to deliver quality custom written papers before the deadline. That's why you don't have to worry about missing the deadline for submitting your assignment.</p>
					</div>
				</li>
				<li>
					<div class="home-cont__why-img">
						<img src="images/svg/icon_book.svg" alt="">
					</div>
					<div class="home-cont__why-text">
						<h3>Free revisions <i class="show-in-mob"></i></h3>
						<p>You can ask to revise your paper as many times as you need until you're completely satisfied with the result. Provide notes about what needs to be changed, and we'll change it right away.</p>
					</div>
				</li>
				<li>
					<div class="home-cont__why-img">
						<img src="images/svg/icon_24h.svg" alt="">
					</div>
					<div class="home-cont__why-text">
						<h3>24/7 Support <i class="show-in-mob"></i></h3>
						<p>From answering simple questions to solving any possible issues, we're always here to help you in chat and on the phone. We've got you covered at any time, day or night.</p>
					</div>
				</li>
			</ul>
		</div>
	</section>
	<section class="home-cont__hiw">
		<div class="def-width">
			<h2 class="like-h1 title">How it works</h2>
			<ul>
				<li>
					<img src="images/svg/icon_1.svg" alt="">
					<p>You submit paper instructions</p>
				</li>
				<li>
					<img src="images/svg/icon_2.svg" alt="">
					<p>We assign the writer</p>
				</li>
				<li>
					<img src="images/svg/icon_3.svg" alt="">
					<p>Writer makes a research and writes your paper</p>
				</li>
				<li>
					<img src="images/svg/icon_4.svg" alt="">
					<p>We send the completed paper to you</p>
				</li>
			</ul>
			<a href="#" class="butn butn-red">Order now</a>
		</div>
	</section>
	<section class="home-cont__stat">
		<div class="def-width">
			<h2 class="like-h1 title">Statistics</h2>
			<ul>
				<li>
					<div><img src="images/svg/icon_notebook_clock.svg" alt=""></div>
					<span id="count-deliv">27 684</span>
					<p>Orders delivered</p>
				</li>
				<li>
					<div><img src="images/svg/icon_book_pencil.svg" alt=""></div>
					<span id="count-online">104</span>
					<p>Writer online now</p>
				</li>
				<li>
					<div><img src="images/svg/icon_notebook_writers.svg" alt=""></div>
					<span id="count-prof">495</span>
					<p>Professional writers</p>
				</li>
				<li>
					<div><img src="images/svg/icon_test.svg" alt=""></div>
					<span><i id="count-aver">5</i>/10</span>
					<p>Average writers experience</p>
				</li>
			</ul>
		</div>
	</section>
	<section class="home-cont__reviews">
		<div class="def-width">
			<h2 class="like-h1 title">Students’ reviews</h2>
			<ul class="home-cont__reviews-slider">
				<li class="slide-blue">
					<h3>Alex wrote on <a href="#">SiteJabber</a></h3>
					<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
					<div class="rating-stars">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
					</div>
				</li>
				<li class="slide-red">
					<h3>Alex wrote on <a href="#">SiteJabber</a></h3>
					<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic."</p>
					<div class="rating-stars">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star2.svg" alt="">
					</div>
				</li>
				<li class="slide-dark">
					<h3>Alex wrote on <a href="#">SiteJabber</a></h3>
					<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete."</p>
					<div class="rating-stars">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
					</div>
				</li>
				<li class="slide-blue">
					<h3>Alex wrote on <a href="#">SiteJabber</a></h3>
					<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
					<div class="rating-stars">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
					</div>
				</li>
				<li class="slide-red">
					<h3>Alex wrote on <a href="#">SiteJabber</a></h3>
					<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
					<div class="rating-stars">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star2.svg" alt="">
					</div>
				</li>
				<li class="slide-dark">
					<h3>Alex wrote on <a href="#">SiteJabber</a></h3>
					<p>"My essay has not only good grammar, but it also demonstrates the thorough analysis and complete understanding of the topic. Awesome!"</p>
					<div class="rating-stars">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
						<img src="images/svg/icon_star.svg" alt="">
					</div>
				</li>
			</ul>
		</div>
	</section>
	<!-- Call to action -->
  <section class="main-cta">
  	<div class="def-width">
  		<h2>Get quality essays without payingupfront</h2>
  		<a href="#" class="butn butn-red">Order now</a>
  	</div>
  </section>

</main>

@endsection