
<!DOCTYPE html>
<html lang="en">
<head>
	<title>EssayAssist</title>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <!-- main CSS -->
  <link rel="stylesheet" href="{{ asset('css/main.css') }}">

</head>
<body>
<!-- Header -->
@include('layouts.partials.header')
<!-- Content -->
@yield('content')
<!-- Footer -->
@include('layouts.partials.footer')
<!-- jQuery -->
<script src="{{ asset('js/jquery-1.12.4.js')}}"></script>
<!-- Small Scripts -->
<script src="{{asset('js/smallScripts.js')}}"></script>
<!-- Count Up -->
<script src="{{asset('js/countUp.js')}}"></script>
<!-- Pagination -->
<script src="{{asset('js/pagination.js')}}"></script>
<!-- Bx Slider -->
<script src="{{ asset('js/jquery.bxslider.js')}}"></script>

	<script>
		// bxSlider
		if ($(window).width() <= 760) {
			$(document).ready(function(){
				$('.home-cont__reviews-slider').bxSlider({
					slideWidth: 316,
					slideMargin: 20,
					minSlides: 1,
					maxSlides: 1,
					auto: true,
					pause: 3000
				});
			});
		}
		if ($(window).width() <= 1100) {
			$(document).ready(function(){
				$('.home-cont__reviews-slider').bxSlider({
					slideWidth: 316,
					slideMargin: 20,
					minSlides: 2,
					maxSlides: 2,
					//auto: true,
					//pause: 2000
				});
			});
		}
		$(document).ready(function(){
			$('.home-cont__reviews-slider').bxSlider({
				slideWidth: 316,
				slideMargin: 30,
				minSlides: 3,
				maxSlides: 3,
				//auto: true,
				//pause: 2000
			});
		});
		// Count number animate
		var options = {
			useEasing : true,
			useGrouping : true,
			separator : ' ',
		};
		var countDeliv = new CountUp("count-deliv", 20000, 27684, 0, 4, options);
		var countOnline = new CountUp("count-online", 50, 104, 0, 4, options);
		var countProf = new CountUp("count-prof", 200, 495, 0, 4, options);
		var countAver = new CountUp("count-aver", 0, 5, 0, 4, options);
		$(window).scroll(function(){
			var wScroll = $(this).scrollTop();
			if(wScroll >= $('.home-cont__hiw').offset().top - 200) {
				countDeliv.start();
				countOnline.start();
				countProf.start();
				countAver.start();
			}
		});
	</script>
</body>
</html>
