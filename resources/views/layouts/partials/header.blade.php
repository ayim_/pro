<header class="home-header">
    <!-- showing in tablet -->
    <div class="header-top show-in-tab">
        <div class="def-width">
            <div class="header-cntry">
                <i>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="4" viewBox="0 0 8 4">
                        <path id="icon-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#fff" />
                    </svg>
                </i>
                <span><img src="{{ asset('images/svg/flag.svg') }}" width="20" height="13" alt=""></span>
                <ul>
                    <li><a href="#"><img src="{{ asset('images/svg/flag.svg') }}" width="20" height="13" alt=""></a></li>
                    <li><a href="#"><img src="{{ asset('images/svg/flag.svg') }}" width="20" height="13" alt=""></a></li>
                </ul>
            </div>
            <div class="header-numbers">
                <i>
                    <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="8" height="4" viewBox="0 0 8 4">
                        <path id="icon-arrow" d="M3.916,3.967 C3.916,3.967 7.250,0.223 7.250,0.223 C7.286,0.184 7.289,0.133 7.261,0.090 C7.232,0.048 7.175,0.021 7.113,0.021 C7.113,0.021 0.445,0.021 0.445,0.021 C0.383,0.021 0.326,0.048 0.298,0.090 C0.285,0.109 0.279,0.130 0.279,0.150 C0.279,0.176 0.289,0.202 0.308,0.223 C0.308,0.223 3.642,3.967 3.642,3.967 C3.673,4.001 3.724,4.022 3.779,4.022 C3.834,4.022 3.885,4.001 3.916,3.967 z" fill="#fff" />
                    </svg>
                </i>
                <span>+1 888 986 0641</span>
                <ul>
                    <li><a href="tel:+18889860641">+1 888 986 0641</a></li>
                    <li><a href="tel:+18889860641">+1 888 986 0641</a></li>
                </ul>
            </div>
            <div class="header-sign">
                <a href="#"><img src="{{ asset('images/svg/icon_sign_in.svg') }}" alt="">Sign in</a>
            </div>
        </div>
    </div>
    <!-- showing in tablet END-->
    <div class="header-cont">
        <div class="def-width">
            <!-- showing in mobile -->
            <div class="header-nav-toggler show-in-mob">
                <img src="{{ asset('images/svg/nav_toggler.svg') }}" alt="">
            </div>
            <!-- showing in mobile END-->
            <div class="header-logo">
                <a href="{{ url('/')}}">
                    Essay Partners
                </a>
            </div>
            <!-- showing in mobile -->
            <div class="header-phone-toggler js-popup-open show-in-mob">
                <img src="{{ asset('images/svg/phone_toggler.svg') }}" alt="">
            </div>
            <!-- showing in mobile END-->
            <nav class="header-nav" id="header-nav">
                <div class="header-nav-toggler show-in-mob">
                    <img src="{{ asset('images/svg/icon_close.svg') }}" alt="">
                </div>
                <ul>
                    <li><a href="{{ url('/')}}">Home</a></li>
                    <li><a href="{{ route('hiw')}}">How it works</a></li>
                    <li><a href="{{ route('samples')}}">Samples</a></li>
                    <li><a href="{{ route('pricing')}}">Pricing</a></li>
                    <li><a href="{{ route('faq')}}">FAQ</a></li>
                    <li><a href="{{ route('about')}}">About</a></li>
                    <li><a href="{{ route('order')}}">Order</a></li>
                </ul>
                <div class="header-sign show-in-mob">
                    <a href="{{ route('login')}}">
                        <img src="{{ asset('images/svg/icon_sign_in.svg') }}" alt="">
                        Sign in
                    </a>
                </div>
            </nav>
            <!-- showing in mobile -->
                <div class="black-back"></div>
            <!-- showing in mobile END-->
            <div class="header-right">
                <div class="header-cntry">
                    <i><img src="{{ asset('images/svg/icon_arrow.svg') }}" alt=""></i>
                    <span><img src="{{ asset('images/svg/flag.svg') }}" width="20" height="13" alt=""></span>
                    <ul>
                        <li><a href="#"><img src="{{ asset('images/svg/flag.svg') }}" width="20" height="13" alt=""></a></li>
                        <li><a href="#"><img src="{{ asset('images/svg/flag.svg') }}" width="20" height="13" alt=""></a></li>
                    </ul>
                </div>
                <div class="header-numbers">
                    <i><img src="{{ asset('images/svg/icon_arrow.svg') }}" alt=""></i>
                    <span>+1 888 986 0641</span>
                    <ul>
                        <li><a href="tel:+18889860641">+1 888 986 0641</a></li>
                        <li><a href="tel:+18889860641">+1 888 986 0641</a></li>
                    </ul>
                </div>
                <div class="header-sign">
                    <a href="{{ route('login')}}"><img src="{{ asset('images/svg/icon_sign_in.svg') }}" alt="">Sign in</a>
                </div>
            </div>
        </div>
    </div>
  
    <!-- popup phone numbers -->
    <div class="popup-phones">
        <div class="popup-frame">
            <div class="popup">
                <div class="popup-inset">
                    <i class="popup-close js-popup-close">
                        <svg version="1.1" xmlns="http://www.w3.org/2000/svg" xmlns:xlink="http://www.w3.org/1999/xlink" width="17" height="17" viewBox="0 0 17 17">
                            <path id="icon-close" d="M7.686,8.502 C7.686,8.502 0.161,16.080 0.161,16.080 C-0.048,16.291 -0.048,16.632 0.161,16.842 C0.266,16.948 0.403,17.000 0.540,17.000 C0.677,17.000 0.814,16.948 0.918,16.842 C0.918,16.842 8.500,9.207 8.500,9.207 C8.500,9.207 16.081,16.842 16.081,16.842 C16.186,16.948 16.323,17.000 16.460,17.000 C16.596,17.000 16.733,16.948 16.838,16.842 C17.047,16.632 17.047,16.291 16.838,16.080 C16.838,16.080 9.313,8.502 9.313,8.502 C9.313,8.502 16.843,0.919 16.843,0.919 C17.052,0.709 17.052,0.367 16.843,0.157 C16.634,-0.053 16.295,-0.053 16.086,0.157 C16.086,0.157 8.500,7.797 8.500,7.797 C8.500,7.797 0.913,0.157 0.913,0.157 C0.704,-0.053 0.365,-0.053 0.156,0.157 C-0.053,0.368 -0.053,0.709 0.156,0.919 C0.156,0.919 7.686,8.502 7.686,8.502 z" fill="#10C0E7" />
                        </svg>
                    </i>
                    <div class="popup-body">
                        <p class="popup-title">Call us Toll free</p>
                        <ul class="popup-list">
                            <li><span>US:</span> <a href="tel:+18889860641">+1 (888) 986 0641</a></li>
                            <li><span>UK:</span> <a href="tel:+18889860641">+1 (888) 986 0641</a></li>
                            <li><span>AU:</span> <a href="tel:+18889860641">+1 (888) 986 0641</a></li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
  
  </header>