<footer>
    <div class="def-width">
      <div class="footer-left">
        <ul>
          <li>
            <img src="{{ asset('images/svg/icon_mail.svg') }} " alt="">
            <a href="mailto:support@essaypartners.com">support@essaypartners.com</a>
          </li>
          <li>
            <img src="{{ asset('images/svg/icon_telephone.svg') }} " alt="">
            <a href="tel:+18554077728">+1 855 407 77 28</a>
          </li>
          <li>
            <img src="{{ asset('images/svg/icon_paypal.svg') }} " alt="">
            <img src="{{ asset('images/svg/icon_mastercard.svg') }} " alt="">
            <img src="{{ asset('images/svg/icon_visa.svg') }} " alt="">
          </li>
        </ul>
      </div>
      <div class="footer-right">
        <nav class="footer-nav">
          <ul>
            <li><a href="{{url('/')}}"">Home</a></li>
            <li><a href="{{ route('hiw')}}">How it works</a></li>
            <li><a href="{{ route('samples')}}">Samples</a></li>
            <li><a href="{{ route('pricing')}}">Pricing</a></li>
            <li><a href="{{ route('faq') }}">FAQ</a></li>
            <li><a href="{{route('about')}}">About</a></li>
            <li><a href="{{route('order')}}">Order</a></li>
          </ul>
        </nav>
        <nav class="footer-more-nav">
          <ul>
            <li><a href="#">Terms and Conditions</a></li>
            <li><a href="#">Refund Policy</a></li>
            <li><a href="#">Privacy Policy</a></li>
            <li><a href="#">Revision Policy</a></li>
          </ul>
        </nav>
        <div class="copyright">{{ date('Y') }} © Essay Partners. All rights reserved</div>
      </div>
    </div>
  </footer>