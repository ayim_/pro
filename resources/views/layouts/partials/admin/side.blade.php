<div class="main-menu menu-fixed menu-light menu-accordion menu-shadow" data-scroll-to-active="true">
    <div class="navbar-header">
        <ul class="nav navbar-nav flex-row">
            <li class="nav-item mr-auto"><a class="navbar-brand" href="{{ url('/')}}">
                    <div class="brand-logo"><img class="logo" src="{{ asset('app-assets/images/logo/logo.png') }}" /></div>
                    <h2 class="brand-text mb-0">EPartners</h2>
                </a></li>
            <li class="nav-item nav-toggle"><a class="nav-link modern-nav-toggle pr-0" data-toggle="collapse"><i class="bx bx-x d-block d-xl-none font-medium-4 primary toggle-icon"></i><i class="toggle-icon bx bx-disc font-medium-4 d-none d-xl-block collapse-toggle-icon primary" data-ticon="bx-disc"></i></a></li>
        </ul>
    </div>
    <div class="shadow-bottom"></div>
    <div class="main-menu-content">
        <ul class="navigation navigation-main" id="main-menu-navigation" data-menu="menu-navigation" data-icon-style="">
            <li class=" nav-item">
                <a href="{{ route('admin.dashboard') }}">
                    <i class="bx bx-home-alt"></i>
                    <span class="menu-title" data-i18n="Dashboard">Dashboard</span>
                </a>
            </li>
            <li class="navigation-header"><span>Apps</span>
            </li>
            <li class=" nav-item"><a href="{{ route('admin.orders.index') }}"><i class="bx bx-envelope"></i><span class="menu-title" data-i18n="Email">Orders</span></a>
            </li>
            <li class="nav-item">
                <a href="{{ route('admin.subjects.index')}}">
                    <i class="bx bx-chat"></i>
                    <span class="menu-title" data-i18n="Subjects">Subjects</span>
                </a>
            </li>
            <li class=" nav-item">
                <a href="{{ route('admin.academic_levels.index')}}">
                    <i class="bx bx-check-circle"></i>
                    <span class="menu-title" data-i18n="Todo">Academic Levels</span>
                </a>
            </li>
            <li class=" nav-item">
                <a href="{{ route('admin.work_types.index')}}">
                    <i class="bx bx-calendar"></i>
                    <span class="menu-title" data-i18n="Calendar">Work Types
                    </span>
                </a>
            </li>
            <li class=" nav-item">
                <a href="{{ route('admin.customers.index')}}">
                    <i class="bx bx-grid-alt"></i>
                    <span class="menu-title" data-i18n="Kanban">Customers</span>
                </a>
            </li>
            <li class="navigation-header"><span>Settings</span>
            </li>

            <li class=" nav-item">
                <a href="{{ route('admin.work_types.index')}}">
                    <i class="bx bx-calendar"></i>
                    <span class="menu-title" data-i18n="Calendar">Staff
                    </span>
                </a>
            </li>
            <li class=" nav-item">
                <a href="{{ route('admin.work_types.index')}}">
                    <i class="bx bx-calendar"></i>
                    <span class="menu-title" data-i18n="Calendar">Settings
                    </span>
                </a>
            </li>
            <li class=" nav-item">
                <a href="{{ route('admin.work_types.index')}}">
                    <i class="bx bx-calendar"></i>
                    <span class="menu-title" data-i18n="Calendar">Blog
                    </span>
                </a>
            </li>

        </ul>
    </div>
</div>
