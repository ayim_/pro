@extends('layouts.auth')

@section('content')

<div class="content-overlay"></div>
        <div class="content-wrapper">
            <div class="content-header row">
            </div>
            <div class="content-body">
                <!-- register section starts -->
                <section class="row flexbox-container">
                    <div class="col-xl-8 col-10">
                        <div class="card bg-authentication mb-0">
                            <div class="row m-0">
                                <!-- register section left -->
                                <div class="col-md-6 col-12 px-0">
                                    <div class="card disable-rounded-right mb-0 p-2 h-100 d-flex justify-content-center">
                                        <div class="card-header pb-1">
                                            <div class="card-title">
                                                <h4 class="text-center mb-2">Sign Up</h4>
                                            </div>
                                        </div>
                                        <div class="text-center">
                                            <p> <small> Please enter your details to sign up and be part of our great community</small>
                                            </p>
                                        </div>
                                        <div class="card-content">
                                            <div class="card-body">
                                                <form action="{{ route('register')}}" method="POST">
                                                    @csrf()
                                                    <div class="form-row">
                                                        <div class="form-group col-md-6 mb-50">
                                                            <label for="inputfirstname4">first name</label>
                                                            <input type="text" class="form-control" id="first_name" name="last_name" placeholder="First name">
                                                        </div>
                                                        <div class="form-group col-md-6 mb-50">
                                                            <label for="inputlastname4">last name</label>
                                                            <input type="text" class="form-control" id="last_name" name="last_name" placeholder="Last name">
                                                        </div>
                                                    </div>
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600" for="exampleInputUsername1">username</label>
                                                        <input type="text" id='username' class="form-control" name="username" placeholder="Username">
                                                    </div>
                                                    <div class="form-row">
                                                        <div class="form-group col-md-2 mb-50">
                                                            <label class="text-bold-600" for="country_code">Code</label>
                                                            <input type="text" id='country_code' class="form-control" name="country_code" placeholder="Code">
                                                        </div>
                                                        <div class="form-group col-md-10 mb-50">
                                                            <label class="text-bold-600" for="phone">phone Number</label>
                                                            <input type="text" id='phone' class="form-control" name="phone" placeholder="Phone Number">
                                                        </div>

                                                    </div>
                                                    <div class="form-group mb-50">
                                                        <label class="text-bold-600" for="exampleInputEmail1">Email address</label>
                                                        <input type="email" class="form-control" id="email" name="email" placeholder="Email address">
                                                    </div>
                                                    <div class="form-group mb-2">
                                                        <label class="text-bold-600" for="exampleInputPassword1">Password</label>
                                                        <input type="password" class="form-control" id="password" name="password" placeholder="Password">
                                                    </div>
                                                    <div class="form-group mb-2">
                                                        <label class="text-bold-600" for="password_confirmation">Confirm Password</label>
                                                        <input type="password" class="form-control" id="password_confirmation" name="password_confiramtion" placeholder="Confirm Password">
                                                    </div>
                                                    <button type="submit" class="btn btn-primary glow position-relative w-100">SIGN UP<i id="icon-arrow" class="bx bx-right-arrow-alt"></i></button>
                                                </form>
                                                <hr>
                                                <div class="text-center"><small class="mr-25">Already have an account?</small><a href="{{ route('login')}}"><small>Sign in</small> </a></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <!-- image section right -->
                                <div class="col-md-6 d-md-block d-none text-center align-self-center p-3">
                                    <img class="img-fluid" src="{{ asset('app-assets/images/pages/register.png')}}" alt="branding logo">
                                </div>
                            </div>
                        </div>
                    </div>
                </section>
                <!-- register section endss -->
            </div>
        </div>

@endsection