@extends('layouts.admin')

@section('content')

    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
            <section id="worktype-index">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                {{ __('All Work Types') }}
                                <a href="{{ route('admin.work_types.index') }}" class="btn btn-light-primary">Types</a>
                            </div>
                            <div class="card-body">
                                <form action="{{ route('admin.work_types.store') }}" method="POST">
                                    @csrf
                                    <div class="form-group">
                                        <label for="name">Name: </label>
                                        <input class="form-control" id="name" name="name" value="{{ old('name') }}">
                                    </div>
                                    <div class="form-group">
                                        <label for="status">Status: </label>
                                        <select class="form-control" id="status" name="status">
                                            <option value="active"> Active </option>
                                            <option value="inactive">Inactive </option>
                                        </select>
                                    </div>

                                    <div class="form-group">
                                        <button type="submit" class="btn btn-light-primary">Add Type</button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>

@endsection
