@extends('layouts.admin')

@section('content')
    <div class="content-wrapper">
        <div class="content-header row"></div>
        <div class="content-body">
            <section id="worktype-index">
                <div class="row">
                    <div class="col-md-12">
                        <div class="card">
                            <div class="card-header">
                                {{ __('All Work Types') }}
                                <a href="{{ route('admin.citation_styles.create') }}" class="btn btn-light-primary">Add Type</a>
                            </div>
                            <div class="card-body">
                                <div class="table-bordered table-striped">
                                    <table class="table">
                                        <thead>
                                        <tr>
                                            <th>#ID</th>
                                            <th>Name</th>
                                            <th>Status</th>
                                            <th>Created At</th>
                                        </tr>
                                        </thead>
                                        <tbody>
                                        @foreach($data as $datum)
                                            <tr>
                                                <td>{{ $datum->id }}</td>
                                                <td>{{ $datum->name }}</td>
                                                <td>{{ $datum->status }}</td>
                                                <td>{{ \Carbon\Carbon::parse($datum->created_at)->diffForHumans() }}</td>
                                                <td>

                                                </td>
                                            </tr>
                                        @endforeach
                                        </tbody>

                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
        </div>
    </div>
@endsection
