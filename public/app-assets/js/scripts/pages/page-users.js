/*=========================================================================================
    File Name: page-customers.js
    Description: Users page
    --------------------------------------------------------------------------------------
    Item Name: Frest HTML Admin Template
    Version: 1.0
    Author: PIXINVENT
    Author URL: http://www.themeforest.net/user/pixinvent
==========================================================================================*/
$(document).ready(function () {

    // variable declaration
    var usersTable;
    var usersDataArray = [];
    // datatable initialization
    if ($("#customers-list-datatable").length > 0) {
        usersTable = $("#customers-list-datatable").DataTable({
            responsive: true,
            'columnDefs': [
                {
                    "orderable": false,
                    "targets": [7]
                }]
        });
    };
    // on click selected customers data from table(page named page-customers-list)
    // to store into local storage to get rendered on second page named page-customers-view
    $(document).on("click", "#customers-list-datatable tr", function () {
        $(this).find("td").each(function () {
            usersDataArray.push($(this).text().trim())
        })
        localStorage.setItem("usersId", usersDataArray[0]);
        localStorage.setItem("usersUsername", usersDataArray[1]);
        localStorage.setItem("usersName", usersDataArray[2]);
        localStorage.setItem("usersVerified", usersDataArray[4]);
        localStorage.setItem("usersRole", usersDataArray[5]);
        localStorage.setItem("usersStatus", usersDataArray[6]);
    })
    // render stored local storage data on page named page-customers-view
    if (localStorage.usersId !== undefined) {
        $(".customers-view-id").html(localStorage.getItem("usersId"));
        $(".customers-view-username").html(localStorage.getItem("usersUsername"));
        $(".customers-view-name").html(localStorage.getItem("usersName"));
        $(".customers-view-verified").html(localStorage.getItem("usersVerified"));
        $(".customers-view-role").html(localStorage.getItem("usersRole"));
        $(".customers-view-status").html(localStorage.getItem("usersStatus"));
        // update badge color on status change
        if ($(".customers-view-status").text() === "Banned") {
            $(".customers-view-status").toggleClass("badge-light-success badge-light-danger")
        }
        // update badge color on status change
        if ($(".customers-view-status").text() === "Close") {
            $(".customers-view-status").toggleClass("badge-light-success badge-light-warning")
        }
    }
    // page customers list verified filter
    $("#customers-list-verified").on("change", function () {
        var usersVerifiedSelect = $("#customers-list-verified").val();
        usersTable.search(usersVerifiedSelect).draw();
    });
    // page customers list role filter
    $("#customers-list-role").on("change", function () {
        var usersRoleSelect = $("#customers-list-role").val();
        // console.log(usersRoleSelect);
        usersTable.search(usersRoleSelect).draw();
    });
    // page customers list status filter
    $("#customers-list-status").on("change", function () {
        var usersStatusSelect = $("#customers-list-status").val();
        // console.log(usersStatusSelect);
        usersTable.search(usersStatusSelect).draw();
    });
    // customers language select
    if ($("#customers-language-select2").length > 0) {
        $("#customers-language-select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    }
    // page customers list clear filter
    $(".customers-list-clear").on("click", function(){
        usersTable.search("").draw();
    })
    // customers music select
    if ($("#customers-music-select2").length > 0) {
        $("#customers-music-select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    }
    // customers movies select
    if ($("#customers-movies-select2").length > 0) {
        $("#customers-movies-select2").select2({
            dropdownAutoWidth: true,
            width: '100%'
        });
    }
    // customers birthdate date
    if ($(".birthdate-picker").length > 0) {
        $('.birthdate-picker').pickadate({
            format: 'mmmm, d, yyyy'
        });
    }
    // Input, Select, Textarea validations except submit button validation initialization
    if ($(".customers-edit").length > 0) {
        $("input,select,textarea").not("[type=submit]").jqBootstrapValidation();
    }
});
