<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use App\Models\Subject;

class SubjectsController extends Controller
{
    protected $subject;

    public function __construct(Subject $subject)
    {
        $this->subject = $subject;
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $subject = $this->subject->newQuery()
            ->get();
        return view('admin.subjects.index', compact($subject));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.subjects.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $this->validate($request, [
            'name' => "required|unique:subjects",
            'status' => "required"
        ]);

        $data = $request->except('_token');

        $this->subject->create($data);

        return redirect()->route('admin.subjects.index');

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $subject = $this->subject->newQuery()
            ->where('id', $id)
            ->first();

        return view('admin.subjects.show', compact($subject));    
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $subject = $this->subject->newQuery()
            ->where('id', $id)
            ->first();
        
        return view('admin.subjects.edit', compact($subject));   
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $subject = $this->subject->newQuery()
            ->where('id', $id)
            ->first();

        $data = $request->except('_token');
        
        $subject->update($data);

        return redirect()->route('admin.subjects.index');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $subject = $this->subject->newQuery()
            ->where('id', $id)
               ->delete();
        
        return redirect()->route('admin.subjects.index');
    }
}
