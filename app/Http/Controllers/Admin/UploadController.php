<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class UploadController extends Controller
{
    public function upload(Request $request)
    {
        $attachment = Storage::putFile('attachments', $request->file('file'));

        return response()->json([
            'name' => $attachment,
            'display_name' => $request->file->getClientOriginalName()
        ]);
    }

    public function remove(Request $request)
    {
        if (Storage::exists($request->name)) {
            Storage::delete($request->name);
        }
    }

    public function download(Request $request)
    {
        try {

            return Storage::download($request->file);
        } catch (\Exception $e) {
            //
            abort(404);
        }
    }
}
