<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\Setting;
use App\Service\LogoUploadService;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Validator;

class SettingController extends Controller
{
    public function __destruct()
    {
        Artisan::call('cache:clear');
    }

    public function general_information()
    {
        $data = [];

        $rec = $this->get_records([
            'company_name',
            'company_phone',
            'company_email',
            'company_address',
            'company_notification_email',
            'time_zone',
            'number_of_revision_allowed'
        ]);

        return view('admin.settings.general', compact('data', 'rec'));
    }

    public function update_general_information(Request  $request)
    {
        $validator = Validator::make($request->all(), [
            'company_name' => 'required',
            'company_phone' => 'required',
            'company_email' => 'required|email',
            'company_address' => 'required',
            'company_notification_email' => 'required|email',
            'time_zone' => 'required',
            'number_of_revisions_allowed' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $this->save_records($request->except(['_token','_method', 'submit']));

        return redirect()->back()->with([
            'success' => [
                'message' => 'Settings Updated Successfully'
            ]
        ]);
    }

    public function currency()
    {
        $data['dropdowns'] = Setting::currency_dropdown();

        $rec = $this->get_records([
            'currency_symbol',
            'digit_grouping_method',
            'decimal_symbol',
            'thousand_separator'
        ]);

        return view('admin.settings.currency', compact('data', 'rec'));
    }

    public function update_currency(Request  $request)
    {
        $validator = Validator::make($request->all(), [
            'currency_symbol' => 'required',
            'digit_grouping_method' => 'required',
            'decimal_symbol' => 'required',
            'thousand_separator' => 'required'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $this->save_records($request->except(['_token','_method', 'submit']));

        return redirect()->back()->with(['success' => ['message' => 'Successfully updated']]);
    }

    public function staff()
    {
        $data['enable_browsing_work'] = [
            'no' => 'No',
            'yes' => 'Yes'
        ];

        $data['staff_payment_types'] = [
            'fixed' => 'Fixed',
            'percentage' => 'Percentage'
        ];

        $rec = $this->get_records([
            'enable_browsing_work',
            'staff_payment_type',
            'staff_payment_amount'
        ]);

        return view('admin.settings.staff', compact('data', 'rec'));
    }

    public function update_staff(Request $request)
    {
        $rules = [
            'enable_browsing_work' => 'required'
        ];

        if ($request->enable_browsing_work == 'yes') {
            $rules['staff_payment_amount'] = 'required|numeric|min:1';
            $rules['staff_payment_type'] = 'required|in:fixed,percentage';
        }

        $validator = Validator::make($request->all(), $rules, [
            'staff_payment_amount.required' => 'Payment amount is required',
            'staff_payment_amount.numeric' => 'Payment amount should be a valid number'
        ]);

        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $this->save_records($request->except(['_token','_method', 'submit']));

        return redirect()->back()->with(['success' => ['message' => 'Successfully updated']]);
    }

    public function logo_page(Request $request)
    {
        return view('admin.settings.logo');
    }

    public function update_logo(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'file' => 'required'
        ]);

        if ($validator->fails()) {

            return response()->json([
                'status'    => 2,
                'msg'       => implode(", ", $validator->errors()->all())

            ], 422);
        }

        return response()->json((new LogoUploadService())->upload($request));

    }

    private function save_records($data, $auto_load_disables = NULL, $sanitize=NULL)
    {
        foreach ($data as $key => $value) {
            $obj = Setting::updateOrCreate([
                'option_key' => $key
            ]);

            if ($sanitize) {
                $obj->option_value = Purifier::clean(trim($value));
            } else {
                $obj->option_value = trim($value);
            }
            if ($auto_load_disables) {
                $obj->auto_load_disabled = TRUE;
            } else {
                $obj->auto_load_disabled = NULL;
            }

            $obj->save();
        }
    }

    private function get_records($keys)
    {
        if (is_array($keys)) {
            $records = Setting::whereIn('option_key', $keys)->get();
        } else {
            $records = Setting::where('option_key', $keys)->get();
        }

        if (count($records) > 0) {
            $records = $records->toArray();
            $rec = new \stdClass();
            foreach ($records as $row) {
                $rec->{$row['option_key']} = $row['option_value'];
            }

            return $rec;
        }

        return NULL;
    }
}
