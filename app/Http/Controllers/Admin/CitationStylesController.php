<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\Models\CitationStyle;
use Illuminate\Http\Request;

class CitationStylesController extends Controller
{
    protected $citations;

    public function __construct(CitationStyle  $citations)
    {
        $this->citations = $citations;
    }

    public function index()
    {
        $data = $this->citations->newQuery()
            ->get();
        return view('admin.citation_style.index', compact('data'));
    }

    public function create()
    {
        return view('admin.citation_style.create');
    }

    public function store(Request  $request)
    {

    }

    public function edit($id)
    {

    }

    public function update($id, Request $request)
    {

    }

    public function destroy($id)
    {

    }
}
