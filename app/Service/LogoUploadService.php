<?php


namespace App\Service;


use App\Models\Setting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Storage;

class LogoUploadService
{
    /**
     * @param Request $request
     * @return array
     */
    public function upload(Request $request)
    {
        try {
            $old_company_logo = Setting::get_setting('company_logo');
            $file = $this->StoreBase64ToFile($request->input('file'));
            Setting::save_settings([
                'company_logo' => $file
            ]);

            if (Storage::disk('public')->exists($old_company_logo)) {
                Storage::disk('public')->delete($old_company_logo);
            }

            return [
                'status' => 1,
                'file_url' => asset(Storage::url($file))
            ];
        } catch (\Exception $e) {
            return [
                'status' => 2,
                'message' => __('Upload Was Not Successful')
            ];
        }
    }

    /**
     * @param $base64data
     * @return string
     */
    public function StoreBase64ToFile($base64data)
    {
        $image = base64_decode($base64data);
        $filename = uniqid(). '_' . time() . '.png';
        $path = 'uploads/'. $filename;
        Storage::disk('public')->put($path, $image, 'public');

        return $path;
    }
}
