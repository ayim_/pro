<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Subject extends Model
{
    protected $table = "subjects";

    protected $fillable = [
        'name',
        'status'
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
