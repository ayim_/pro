<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class AcademicLevel extends Model
{
    protected $table = "academic_levels";

    protected $fillable = [
        'name',
        'status',
    ];

    public function orders()
    {
        return $this->hasMmany(Order::class, 'order_id', 'id');
    }


}
