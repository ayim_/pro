<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
    protected $table = 'orders';

    protected $guarded = [];

    public function academic_level()
    {
        return $this->belongsTo(AcademicLevel::class);
    }

    public function citation_style()
    {
        return $this->belongsTo(CitationStyle::class);
    }

    public function subject()
    {
        return $this->belongsTo(Subject::class);
    }

    public function work_type()
    {
        return $this->belongsTo(WorkType::class);
    }

}
