<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class CitationStyle extends Model
{
    protected $table = 'citation_styles';

    protected $fillable = [
        'name',
        'status'
    ];

    public function order()
    {
        return $this->hasMany(Order::class);
    }
}
